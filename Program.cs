﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnetServerDll;

/*   
*   If program crashes with "BadImageFormatException"
*   Please, compile it for x64
*   If your program crashes with Exception thrown: 'System.DllNotFoundException' o 'no se ha podido cargar' in UNETServerAssembly.dll
*   Please, copy UnetServerDll to the folder where your exe file is located
*/

public abstract class Bench
{
	
	protected const string ip = "127.0.0.1";
	protected static ushort port = 0;
	protected static ushort maxClients = 0;
	protected static int serverTickRate = 0;
	protected static int clientTickRate = 0;
	protected static int sendRate = 0;
	protected static int reliableMessages = 0;
	protected static int unreliableMessages = 0;

	protected static string message = String.Empty;
    protected static char[] reversedMessage;
    protected static byte[] messageData;
	protected static byte[] reversedData;

	protected static bool processActive = false;
	protected static bool processCompleted = false;
	protected static bool processOverload = false;
	protected static bool processFailure = false;

    protected static bool maxClientsPass = true;
	protected static Thread serverThread;

	protected static volatile int clientsStartedCount = 0;
	protected static volatile int clientsConnectedCount = 0;
	protected static volatile int clientsStreamsCount = 0;
	protected static volatile int clientsDisconnectedCount = 0;
	protected static volatile int serverReliableSent = 0;
	protected static volatile int serverReliableReceived = 0;
	protected static volatile int serverReliableBytesSent = 0;
	protected static volatile int serverReliableBytesReceived = 0;
	protected static volatile int serverUnreliableSent = 0;
	protected static volatile int serverUnreliableReceived = 0;
	protected static volatile int serverUnreliableBytesSent = 0;
	protected static volatile int serverUnreliableBytesReceived = 0;
	protected static volatile int clientsReliableSent = 0;
	protected static volatile int clientsReliableReceived = 0;
	protected static volatile int clientsReliableBytesSent = 0;
	protected static volatile int clientsReliableBytesReceived = 0;
	protected static volatile int clientsUnreliableSent = 0;
	protected static volatile int clientsUnreliableReceived = 0;
	protected static volatile int clientsUnreliableBytesSent = 0;
	protected static volatile int clientsUnreliableBytesReceived = 0;


	private static ushort maxPeers = 0;

	private static readonly Func<int, string> Space = (value) => (String.Empty.PadRight(value));
	private static readonly Func<int, decimal, decimal, decimal> PayloadFlow = (clientsStreamsCount, messageLength, sendRate) => (clientsStreamsCount * (messageLength * sendRate * 2) * 8 / (1000 * 1000)) * 2;


    

    private static void Main(string[] arguments)
	{
		Console.Title = "benchomarko";
		Console.SetIn(new StreamReader(Console.OpenStandardInput(8192), Console.InputEncoding, false, bufferSize: 1024));

	Start:
		Console.WriteLine("Welcome to benchomarko! IP={0} needs to be changed?", ip );
		serverThread = new Thread(UNetBenchmark.Server);

		if (serverThread == null)
		{
			Console.Clear();
			goto Start;
		}

		const ushort defaultPort = 9500;
		const ushort defaultMaxClients = 10;
		const int defaultServerTickRate = 64;
		const int defaultClientTickRate = 64;
		const int defaultSendRate = 15;
		const int defaultReliableMessages = 500;
		const int defaultUnreliableMessages = 1000;
		const string defaultMessage = "Test Nombre Test Posicion 999.999.999-999.999.999";

		Console.Write("Port (default " + defaultPort + "): ");
		UInt16.TryParse(Console.ReadLine(), out port);

		Console.Write("Simulated clients (default " + defaultMaxClients + "): ");
		UInt16.TryParse(Console.ReadLine(), out maxClients);

		Console.Write("Server tick rate (default " + defaultServerTickRate + "): ");
		Int32.TryParse(Console.ReadLine(), out serverTickRate);

		Console.Write("Client tick rate (default " + defaultClientTickRate + "): ");
		Int32.TryParse(Console.ReadLine(), out clientTickRate);

		Console.Write("Client send rate (default " + defaultSendRate + "): ");
		Int32.TryParse(Console.ReadLine(), out sendRate);

		Console.Write("Reliable msgs per client (default " + defaultReliableMessages + "): ");
		Int32.TryParse(Console.ReadLine(), out reliableMessages);

		Console.Write("Unreliable msgs per client (default " + defaultUnreliableMessages + "): ");
		Int32.TryParse(Console.ReadLine(), out unreliableMessages);

		Console.Write("Message (default " + defaultMessage.Length + " characters): ");
		message = Console.ReadLine();

        Console.Write("Begin?");
        Console.ReadLine();
		

		if (port == 0)              { port = defaultPort; }
        if (maxClients == 0)        { maxClients = defaultMaxClients; }
       
        if (serverTickRate == 0)    { serverTickRate = defaultServerTickRate;	}
        if (clientTickRate == 0)    { clientTickRate = defaultClientTickRate;	}
		if (sendRate == 0)          { sendRate = defaultSendRate;	}
		if (reliableMessages == 0)  { reliableMessages = defaultReliableMessages;	}
		if (unreliableMessages == 0){ unreliableMessages = defaultUnreliableMessages;}
		if (message.Length == 0)    { message = defaultMessage;	}

        reversedMessage = message.ToCharArray();
        Array.Reverse(reversedMessage);
        messageData = Encoding.ASCII.GetBytes(message);
		reversedData = Encoding.ASCII.GetBytes(new string(reversedMessage));

		Console.CursorVisible = false;
		Console.Clear();

		processActive = true;

		maxPeers = ushort.MaxValue - 1;
		maxClientsPass = maxClients <= maxPeers;

		if (!maxClientsPass) {	maxClients = Math.Min(Math.Max((ushort)1, (ushort)maxClients), maxPeers);	}

        serverThread.Priority = ThreadPriority.AboveNormal;
		serverThread.Start();
		Thread.Sleep(100);

        Task infoTask = Info();
        Task superviseTask = Supervise();
        Task spawnTask = Spawn();
        
        Console.ReadKey();

        processActive = false;
        Console.WriteLine("Generating Crash...");
        
        Environment.Exit(1);



	}

	private static async Task Info()
	{
        await Task.Factory.StartNew(() =>
        {
            int spinnerTimer = 0;
            int spinnerSequence = 0;
            string space = Space(10);
            string[] spinner = {
                    "/",
                    "—",
                    "\\",
                    "|"
                };
            string[] status = {
                    "Running" + Space(2),
                    "Failure" + Space(2),
                    "Overload" + Space(1),
                    "Completed"
                };
            string[] strings = {
                    "Benchmarking UNET..." + ip,
                    "Server tick rate: " + serverTickRate + ", Client tick rate: " + clientTickRate + " (ticks per second)",
                    maxClients + " clients, " + reliableMessages + " reliable and " + unreliableMessages + " unreliable msgs per client",
                    messageData.Length + " bytes per message, " + sendRate + " msgs per second",
                    "GC mode: " + (!GCSettings.IsServerGC ? "Workstation" : "Server"),
                    "This networking library doesn't support more than " +  maxPeers.ToString() + " peers per server!",
                    "The process is performing in Sustained Low Latency mode.",
                };

            for (int i = 0; i < spinner.Length; i++)
            {
                spinner[i] = Environment.NewLine + "Press any key to stop the process" + Space(1) + spinner[i];
            }

            StringBuilder info = new StringBuilder(1024);
            Stopwatch elapsedTime = Stopwatch.StartNew();

            while (processActive)
            {

                Console.CursorVisible = false;
                Console.SetCursorPosition(0, 0);
                Console.WriteLine(strings[0]);
                Console.WriteLine(strings[1]);
                Console.WriteLine(strings[2]);
                Console.WriteLine(strings[3]);
                Console.WriteLine(strings[4]);


                if (!maxClientsPass)
                {

                    Console.WriteLine();

                }

                if (!maxClientsPass)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(strings[4]);
                    Console.ResetColor();
                }

                
                info.Clear();
                info.AppendLine().Append("Server status: ").Append((processFailure || !serverThread.IsAlive ? status[1] : (processOverload ? status[2] : (processCompleted ? status[3] : status[0]))));
                info.AppendLine().Append("Clients status: ").Append(clientsStartedCount).Append(" started, ").Append(clientsConnectedCount).Append(" connected, ").Append(clientsDisconnectedCount).Append(" dropped");
                info.AppendLine().Append("Server payload flow: ").Append(PayloadFlow(clientsStreamsCount, messageData.Length, sendRate).ToString("0.00")).Append(" mbps \\ ").Append(PayloadFlow(maxClients * 2, messageData.Length, sendRate).ToString("0.00")).Append(" mbps").Append(space);
                info.AppendLine().Append("Clients sent -> Reliable: ").Append(clientsReliableSent).Append(" msgs")
                .Append(", Unreliable: ").Append(clientsUnreliableSent).Append(" msgs");
                info.AppendLine().Append("Server received <- Reliable: ").Append(serverReliableReceived).Append(" msgs").Append(", Unreliable: ").Append(serverUnreliableReceived)
                    .Append(" msgs");
                info.AppendLine().Append("Server sent -> Reliable: ").Append(serverReliableSent).Append(" msgs").Append(", Unreliable: ").Append(serverUnreliableSent)
                    .Append(" msgs");
                info.AppendLine().Append("Clients received <- Reliable: ").Append(clientsReliableReceived).Append(" msgs").Append(", Unreliable: ").Append(clientsUnreliableReceived)
                    .Append(" msgs");

                info.AppendLine().AppendLine().Append("Total=>Reliable: ").Append((ulong)clientsReliableSent + (ulong)serverReliableReceived + (ulong)serverReliableSent + (ulong)clientsReliableReceived)
                .Append(" msgs | ").Append((((ulong)clientsReliableBytesSent + (ulong)serverReliableBytesReceived + (ulong)serverReliableBytesSent + (ulong)clientsReliableBytesReceived)/1000))
                .Append(" KiB, Unreliable:").Append((ulong)clientsUnreliableSent + (ulong)serverUnreliableReceived + (ulong)serverUnreliableSent + (ulong)clientsUnreliableReceived)
                .Append(" msgs | ")
                .Append(((ulong)clientsUnreliableBytesSent + (ulong)serverUnreliableBytesReceived + (ulong)serverUnreliableBytesSent + (ulong)clientsUnreliableBytesReceived) / 1000)
                .Append("KiB ");

		        info.AppendLine().Append("Expected=> Reliable: ").Append(maxClients * (ulong)reliableMessages * 4)
                    .Append(" msgs | ").Append((maxClients * (ulong)reliableMessages * (ulong)messageData.Length * 4)/1000).Append("KiB, Unreliable:").Append(maxClients * (ulong)unreliableMessages * 4)
                    .Append(" msgs | ").Append((maxClients * (ulong)unreliableMessages * (ulong)messageData.Length * 4)/1000).Append(" KiB");


                info.AppendLine().AppendLine().Append("Elapsed time: ").Append(elapsedTime.Elapsed.Hours.ToString("00")).Append(":").Append(elapsedTime.Elapsed.Minutes.ToString("00")).Append(":").Append(elapsedTime.Elapsed.Seconds.ToString("00"));



                Console.WriteLine(info);

				if (spinnerTimer >= 10)
				{
					spinnerSequence++;
					spinnerTimer = 0;
				}
				else
				{
					spinnerTimer++;
				}

				switch (spinnerSequence % 4)
				{
					case 0: Console.WriteLine(spinner[0]);
						break;
					case 1: Console.WriteLine(spinner[1]);
						break;
					case 2: Console.WriteLine(spinner[2]);
						break;
					case 3: Console.WriteLine(spinner[3]);
						break;
				}

				Thread.Sleep(15);
			}

			elapsedTime.Stop();

			if (!processActive && processCompleted)
			{
				Console.SetCursorPosition(0, Console.CursorTop - 1);
				Console.WriteLine("Process COMPLETED! You Can Close Program...");
              
                

			}



		}, TaskCreationOptions.LongRunning);




	}

	private static async Task Supervise()
	{
		await Task.Factory.StartNew(() =>
		{
			decimal currentData = 0;
			decimal lastData = 0;

			while (processActive)
			{
				Thread.Sleep(1000);

				currentData = ((decimal)serverReliableSent + (decimal)serverReliableReceived + (decimal)serverUnreliableSent + (decimal)serverUnreliableReceived + (decimal)clientsReliableSent + (decimal)clientsReliableReceived + (decimal)clientsUnreliableSent + (decimal)clientsUnreliableReceived);

				if (currentData == lastData)
				{
					if (currentData == 0)
					{

						processFailure = true;

					}
					else if (clientsDisconnectedCount > 1 || ((currentData / (maxClients * ((decimal)reliableMessages + (decimal)unreliableMessages) * 4)) * 100) < 90)
					{

						processOverload = true;

					}

					processCompleted = true;
					Thread.Sleep(100);
					processActive = false;

					break;
				}

				lastData = currentData;
			}


            

		}, TaskCreationOptions.LongRunning);
	}

	private static async Task Spawn()
	{
		await Task.Factory.StartNew(() =>
		{
			Task[] clients = new Task[maxClients];

			for (int i = 0; i < maxClients; i++)
			{
				if (!processActive)
					break;


				clients[i] = UNetBenchmark.Client();


				Interlocked.Increment(ref clientsStartedCount);
				Thread.Sleep(15);


			}



		}, TaskCreationOptions.LongRunning);




	}




}


public sealed class UNetBenchmark : Bench
{


	public static void Server() 
	{


        GlobalConfig globalConfig = new GlobalConfig()
        {
            ThreadPoolSize = 4,
            ThreadAwakeTimeout = (uint)serverTickRate,
            MaxHosts = 1,
            MaxPacketSize = (ushort)((messageData.Length * 2) + 32),
            ReactorMaximumSentMessages = (ushort)((reliableMessages + unreliableMessages) * sendRate),
            ReactorMaximumReceivedMessages = (ushort)((reliableMessages + unreliableMessages) * sendRate)

        };
        
		//globalConfig.ReactorMaximumReceivedMessages =;

		ConnectionConfig connectionConfig = new ConnectionConfig();

		int reliableChannel = connectionConfig.AddChannel(QosType.ReliableSequenced);
		int unreliableChannel = connectionConfig.AddChannel(QosType.UnreliableSequenced);

		connectionConfig.SendDelay = 1;
		connectionConfig.MinUpdateTimeout = 1;
		connectionConfig.PingTimeout = 2000;
		connectionConfig.DisconnectTimeout = 5000;
		connectionConfig.PacketSize = (ushort)((messageData.Length * 2) + 32);

        HostTopology topology = new HostTopology(connectionConfig, maxClients)
        {
            SentMessagePoolSize = (ushort)((reliableMessages + unreliableMessages) * sendRate),
            ReceivedMessagePoolSize = (ushort)((reliableMessages + unreliableMessages) * sendRate)
        };


        NetLibraryManager server = new NetLibraryManager(globalConfig);

		int host = server.AddHost(topology, port, ip);
		NetworkEventType netEvent;
		byte[] buffer = new byte[1024];

		

		while (processActive) 
		{

			while ((netEvent = server.Receive(out int hostID, out int connectionID, out int channelID, buffer, buffer.Length, out int dataLength, out byte netError)) != NetworkEventType.Nothing)
            {
				switch (netEvent)
                {
					case NetworkEventType.DataEvent: //solo nos interesa 
                    {


						if (channelID == 0)
                        {
							Interlocked.Increment(ref serverReliableReceived);
							Interlocked.Add(ref serverReliableBytesReceived, dataLength);
							server.Send(hostID, connectionID, reliableChannel, messageData, messageData.Length, out byte sendError);
							Interlocked.Increment(ref serverReliableSent);
							Interlocked.Add(ref serverReliableBytesSent, messageData.Length);
						}
                        else if (channelID == 1)
                        {
							Interlocked.Increment(ref serverUnreliableReceived);
							Interlocked.Add(ref serverUnreliableBytesReceived, dataLength);
							server.Send(hostID, connectionID, unreliableChannel, reversedData, reversedData.Length, out byte sendError);
							Interlocked.Increment(ref serverUnreliableSent);
							Interlocked.Add(ref serverUnreliableBytesSent, reversedData.Length);
						}



                    }
					break;
				}



			}

			Thread.Sleep(1000 / serverTickRate);
		}



	}

	public static async Task Client() 
	{


		await Task.Factory.StartNew(() => 
        {
            GlobalConfig globalConfig = new GlobalConfig()
            {
                ThreadPoolSize = 1,
                ThreadAwakeTimeout = (uint)clientTickRate,
                MaxHosts = 1,
                MaxPacketSize = (ushort)((messageData.Length * 2) + 32),
                ReactorMaximumSentMessages = (ushort)(sendRate / 2),
                ReactorMaximumReceivedMessages = (ushort)(sendRate / 2)
            };


            ConnectionConfig connectionConfig = new ConnectionConfig()
            {
                SendDelay = 1,
                MinUpdateTimeout = 1,
                PingTimeout = 2000,
                DisconnectTimeout = 5000,
                PacketSize = (ushort)((messageData.Length * 2) + 32)
            };


            int reliableChannel = connectionConfig.AddChannel(QosType.ReliableSequenced);
			int unreliableChannel = connectionConfig.AddChannel(QosType.UnreliableSequenced);


            HostTopology topology = new HostTopology(connectionConfig, 1)
            {
                SentMessagePoolSize = (ushort)(sendRate / 2),
                ReceivedMessagePoolSize = (ushort)(sendRate / 2)
            };


            NetLibraryManager client = new NetLibraryManager(globalConfig);

			int host = client.AddHost(topology, 0, null);
			int connection = client.Connect(host, ip, port, 0, out byte connectionError);

			int reliableToSend = 0;
			int unreliableToSend = 0;
			int reliableSentCount = 0;
			int unreliableSentCount = 0;

			Task.Factory.StartNew(async() => 
            {
				bool reliableIncremented = false;
				bool unreliableIncremented = false;

				while (processActive)
                {
					if (reliableToSend > 0)
                    {
						client.Send(host, connection, reliableChannel, messageData, messageData.Length, out byte sendError);
						Interlocked.Decrement(ref reliableToSend);
						Interlocked.Increment(ref reliableSentCount);
						Interlocked.Increment(ref clientsReliableSent);
						Interlocked.Add(ref clientsReliableBytesSent, messageData.Length);
					}

					if (unreliableToSend > 0)
                    {
						client.Send(host, connection, unreliableChannel, reversedData, reversedData.Length, out byte sendError);
						Interlocked.Decrement(ref unreliableToSend);
						Interlocked.Increment(ref unreliableSentCount);
						Interlocked.Increment(ref clientsUnreliableSent);
						Interlocked.Add(ref clientsUnreliableBytesSent, reversedData.Length);
					}

					if (reliableToSend > 0 && !reliableIncremented)
                    {
						reliableIncremented = true;
						Interlocked.Increment(ref clientsStreamsCount);
					}
                    else if (reliableToSend == 0 && reliableIncremented)
                    {
						reliableIncremented = false;
						Interlocked.Decrement(ref clientsStreamsCount);
					}

					if (unreliableToSend > 0 && !unreliableIncremented)
                    {
						unreliableIncremented = true;
						Interlocked.Increment(ref clientsStreamsCount);
					}
                    else if (unreliableToSend == 0 && unreliableIncremented)
                    {
						unreliableIncremented = false;
						Interlocked.Decrement(ref clientsStreamsCount);
					}

					await Task.Delay(1000 / sendRate);
				}
			}, TaskCreationOptions.AttachedToParent);

			byte[] buffer = new byte[1024];
			NetworkEventType netEvent;

			while (processActive)
            {
				while ((netEvent = client.Receive(out int hostID, out int connectionID, out int channelID, buffer, buffer.Length, out int dataLength, out byte netError)) != NetworkEventType.Nothing)
                {

                    

					switch (netEvent)
                    {
						case NetworkEventType.ConnectEvent:

                            Interlocked.Increment(ref clientsConnectedCount);
							Interlocked.Exchange(ref reliableToSend, reliableMessages);
							Interlocked.Exchange(ref unreliableToSend, unreliableMessages);

					    break;

						case NetworkEventType.DisconnectEvent:

                            Interlocked.Increment(ref clientsDisconnectedCount);
							Interlocked.Exchange(ref reliableToSend, 0);
							Interlocked.Exchange(ref unreliableToSend, 0);

						break;

						case NetworkEventType.DataEvent:

                            if (channelID == 0)
                            {
								Interlocked.Increment(ref clientsReliableReceived);
								Interlocked.Add(ref clientsReliableBytesReceived, dataLength);
							}
                            else if (channelID == 1)
                            {
								Interlocked.Increment(ref clientsUnreliableReceived);
								Interlocked.Add(ref clientsUnreliableBytesReceived, dataLength);
							}

						break;
					}
				}

				Thread.Sleep(1000 / clientTickRate);
			}

			client.Disconnect(host, connection, out byte disconnectError);




		}, TaskCreationOptions.LongRunning);
	}





}





